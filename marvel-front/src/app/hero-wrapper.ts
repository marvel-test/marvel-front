import { Hero } from './hero';

export interface HeroWrapper {
    results: Hero[];
    total: number;
    attributionHTML: string;
}