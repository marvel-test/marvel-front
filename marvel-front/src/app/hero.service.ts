import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HeroWrapper } from './hero-wrapper';
import { HEROES } from './mock-heroes';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private http: HttpClient) { }

  private heroesUrl = 'http://localhost:8080/character?offset=0';  // URL to web api

  getHeroes(): Observable<HeroWrapper> {
    return this.http.get<HeroWrapper>(this.heroesUrl)
      .pipe(
        catchError(this.handleError<HeroWrapper>('getHeroes', null))
      );
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
