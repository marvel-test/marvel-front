export interface Hero {
    id: number;
    name: string;
    thumbnail: string;
    description: string;
}